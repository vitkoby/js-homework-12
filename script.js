'use strict';

/* Ответы на теоритические вопросы: 
1. Пользователь может вводить символы с разных устройств, отображение может быть не всемгда корректным. */

const wrapperBtn = document.querySelector('.btn-wrapper');
const btnFirst = document.querySelector('.btn-first');
const btnSecond = document.querySelector('.btn-second');
const btnThird = document.querySelector('.btn-third');
const btnFourth = document.querySelector('.btn-fourth');
const btnFifth = document.querySelector('.btn-fifth');
const btnSixth = document.querySelector('.btn-sixth');
const btnSeventh = document.querySelector('.btn-seventh');


document.addEventListener('keydown', e => {
	const target = e.code;
	console.log(target);

	for (let item of wrapperBtn.children) {
		if (target !== 'Enter') {
			btnFirst.classList.add('active');
		}
	} 
	btnFirst.classList.toggle('active');

	for (let item of wrapperBtn.children) {
		if (target !== 'KeyS') {
			btnSecond.classList.add('active');
		}
	} 
	btnSecond.classList.toggle('active');

	for (let item of wrapperBtn.children) {
		if (target !== 'KeyE') {
			btnThird.classList.add('active');
		}
	} 
	btnThird.classList.toggle('active');

	for (let item of wrapperBtn.children) {
		if (target !== 'KeyO') {
			btnFourth.classList.add('active');
		}
	} 
	btnFourth.classList.toggle('active');

	for (let item of wrapperBtn.children) {
		if (target !== 'KeyN') {
			btnFifth.classList.add('active');
		}
	} 
	btnFifth.classList.toggle('active');

	for (let item of wrapperBtn.children) {
		if (target !== 'KeyL') {
			btnSixth.classList.add('active');
		}
	} 
	btnSixth.classList.toggle('active');

	for (let item of wrapperBtn.children) {
		if (target !== 'KeyZ') {
			btnSeventh.classList.add('active');
		}
	} 
	btnSeventh.classList.toggle('active');
});



//    По клику мыши

/* wrapperBtn.addEventListener('click', e => {
	const target = e.target;

	for (let item of wrapperBtn.children) {
		if (item === target) {
			item.classList.add('active');
		} else {
			item.classList.remove('active');
		}
	}
}); */